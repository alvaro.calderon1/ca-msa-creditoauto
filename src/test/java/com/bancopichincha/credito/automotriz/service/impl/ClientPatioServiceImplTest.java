package com.bancopichincha.credito.automotriz.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bancopichincha.credito.automotriz.domain.Client;
import com.bancopichincha.credito.automotriz.domain.ClientPatioRelationship;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.repository.ClientPatioRepository;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.CreditSolicitudeRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.service.ClientPatioService;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import com.bancopichincha.credito.automotriz.service.dto.ClientPatioDto;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.mapper.ClientPatioRelationshipMapper;
import com.bancopichincha.credito.automotriz.service.mapper.ClientPatioRelationshipMapperImpl;
import com.bancopichincha.credito.automotriz.service.mapper.PatioMapperImpl;
import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

class ClientPatioServiceImplTest {

  ClientPatioRepository clientPatioRepository;

  ClientRepository clientRepository;
  PatioRepository patioRepository;
  ClientPatioRelationshipMapper mapper;
  ClientPatioService clientPatioService;
  PatioService patioService;

  CreditSolicitudeRepository creditSolicitudeRepository;

  public ClientPatioServiceImplTest() {
    clientPatioRepository = mock(ClientPatioRepository.class);
    clientRepository = mock(ClientRepository.class);
    patioRepository = mock(PatioRepository.class);
    creditSolicitudeRepository = mock(CreditSolicitudeRepository.class);
    patioService = new PatioServiceImpl(patioRepository, new PatioMapperImpl());
    this.mapper = new ClientPatioRelationshipMapperImpl();
    clientPatioService = new ClientPatioServiceImpl(clientPatioRepository, clientRepository, mapper,
        creditSolicitudeRepository, patioRepository);
  }

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  private ClientPatioRelationship getClientsPatioRelationship(Long relationshipId) {
    return ClientPatioRelationship.builder().id(relationshipId).build();
  }

  @Nested
  @DisplayName("- Get Client-Patio relation")
  class GetClientsPatioRelationship {

    @Test
    @DisplayName("When id is null then throw IllegalArgumentException")
    public void exceptionWhenIdNull() {
      //given
      Long relationshipId = null;
      //when
      when(clientPatioRepository.findById(relationshipId)).thenThrow(
          new IllegalArgumentException("El id no puede ser nulo"));
      //then
      IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.getClientsPatioRelationship(relationshipId));
      assertEquals("El id no puede ser nulo", exception.getMessage());
    }

    @Test
    @DisplayName("When id is not in database then throw EntityNotFoundException")
    public void exceptionWhenIdNotFound() {
      //given
      Long relationshipId = anyLong();
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(Optional.empty());
      //then
      Exception ex = assertThrows(EntityNotFoundException.class, () -> {
        clientPatioService.getClientsPatioRelationship(relationshipId);
      });
      assertEquals("La relacion patio cliente no existe", ex.getMessage());
    }

    @Test
    @DisplayName("When id is not null and client is found in database then return AssignClientPatioDto")
    public void returnAssignClientPatioDto() {
      //given
      Long relationshipId = anyLong();
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(Optional.of(
          getClientsPatioRelationship(relationshipId)));
      //then
      ClientPatioDto clientPatioDto = clientPatioService.getClientsPatioRelationship(
          relationshipId);
      assertEquals(clientPatioDto.getId(), relationshipId);
    }
  }

  @Nested
  @DisplayName("- Save Client-Patio relation")
  class SaveClientPatioRelation {

    public ClientPatioDto generateAssignClientPatioDtoClientIdNull() {
      return ClientPatioDto.builder()
          .assignedAt("2021-01-01")
          .clientDto(ClientDto.builder().id(null).build())
          .build();
    }

    public ClientPatioDto generateAssignClientPatioDtoPatioIdNull() {
      return ClientPatioDto.builder()
          .assignedAt("2021-01-01")
          .patioDto(PatioDto.builder().id(null).build())
          .clientDto(ClientDto.builder().id(1L).build())
          .build();
    }

    public ClientPatioDto generateValidAssignClientPatioDto() {
      return ClientPatioDto.builder()
          .assignedAt("2021-01-01")
          .patioDto(PatioDto.builder().id(1L).build())
          .clientDto(ClientDto.builder().id(1L).build())
          .build();
    }

    @Test
    @DisplayName("When ClientDto.id is null then throw IllegalArgumentException")
    public void clientIdNull() {
      //Given
      ClientPatioDto clientPatioDto = generateAssignClientPatioDtoClientIdNull();
      //when
      when(clientRepository.findById(null)).thenThrow(
          new RuntimeException("El id del cliente no puede ser nulo"));
      //then
      Exception ex = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.assignClientToPatio(clientPatioDto));
      assertEquals("El id del cliente no puede ser nulo", ex.getMessage());
    }

    @Test
    @DisplayName("When PatioDto.id is null then throw IllegalArgumentException")
    public void clientDtoNull() {
      //Given
      ClientPatioDto clientPatioDto = generateAssignClientPatioDtoPatioIdNull();
      //when
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
      when(patioRepository.findById(null)).thenThrow(
          new RuntimeException("El id del patio no puede ser nulo"));
      //then
      Exception ex = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.assignClientToPatio(clientPatioDto));
      assertEquals("El id del patio no puede ser nulo", ex.getMessage());
    }

    @Test
    @DisplayName("When not client not found in database then return EntityNotFoundException")
    public void clientNotFound() {
      //Given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto();
      //then
      when(clientPatioRepository.save(any(ClientPatioRelationship.class))).thenReturn(
          getClientsPatioRelationship(5L));
      when(clientRepository.existsById(anyLong())).thenReturn(false);
      //when
      Exception ex = assertThrows(EntityNotFoundException.class,
          () -> clientPatioService.assignClientToPatio(clientPatioDto));
      assertEquals("El cliente no existe", ex.getMessage());
    }

    @Test
    @DisplayName("When not patio not found in database then return EntityNotFoundException")
    public void patioNotFound() {
      //Given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto();
      //then
      when(clientPatioRepository.save(any(ClientPatioRelationship.class))).thenReturn(
          getClientsPatioRelationship(5L));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
      when(patioRepository.findById(anyLong())).thenReturn(
          Optional.empty());
      //when
      Exception ex = assertThrows(EntityNotFoundException.class,
          () -> clientPatioService.assignClientToPatio(clientPatioDto));
      assertEquals("El patio no existe", ex.getMessage());
    }

    @Test
    @DisplayName("When saving a client-patio relationship then return AssignClientPatioDto")
    public void saveClientPatioRelation() {
      //Given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto();
      //then
      when(clientPatioRepository.save(any(ClientPatioRelationship.class))).thenReturn(
          getClientsPatioRelationship(5L));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
      when(patioRepository.findById(anyLong())).thenReturn(
          Optional.of(Patio.builder().id(5L).build()));
      //when
      var result = clientPatioService.assignClientToPatio(clientPatioDto);
      assertEquals(result.getId(), 5L);
    }
  }

  @Nested
  @DisplayName("- Delete Client-Patio relation")
  class DeleteClientPatioRelationship {

    public ClientPatioRelationship generateAssignClientPatio() {
      Client client = new Client();
      client.setId(1L);

      return ClientPatioRelationship.builder()
          .id(1L)
          .assignedAt(LocalDate.now())
          .patio(Patio.builder().id(1L).build())
          .client(client)
          .build();
    }

    @Test
    @DisplayName("When id is null then throw IllegalArgumentException")
    public void exceptionWhenIdNull() {
      //given
      Long relationshipId = null;
      //when
      when(clientPatioRepository.findById(relationshipId)).thenThrow(
          new IllegalArgumentException("El id no puede ser nulo"));
      //then
      IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.deleteClientPatioRelationship(relationshipId));
      assertEquals("El id no puede ser nulo", exception.getMessage());
      verify(clientPatioRepository, times(1)).findById(relationshipId);
      verify(clientPatioRepository, times(0)).deleteById(relationshipId);
    }

    @Test
    @DisplayName("When id is not in database then throw EntityNotFoundException")
    public void exceptionWhenIdNotFound() {
      //given
      Long relationshipId = anyLong();
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(Optional.empty());
      //then
      Exception ex = assertThrows(EntityNotFoundException.class, () -> {
        clientPatioService.deleteClientPatioRelationship(relationshipId);
      });
      assertEquals("La relacion patio cliente no existe", ex.getMessage());
      verify(clientPatioRepository, times(1)).findById(relationshipId);
      verify(clientPatioRepository, times(0)).deleteById(relationshipId);
    }

    @Test
    @DisplayName("When there is related data then throw ConflictException")
    public void exceptionWhenRelatedData() {
      //given
      Long relationshipId = anyLong();
      ClientPatioRelationship assignClientPatio = generateAssignClientPatio();
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(
          Optional.of(assignClientPatio));
      when(creditSolicitudeRepository.countByClient_IdAndPatio_IdAndStatusRegistered(anyLong(),
          anyLong()))
          .thenReturn(1L);
      //then
      Exception ex = assertThrows(ConflictOperationException.class, () -> {
        clientPatioService.deleteClientPatioRelationship(relationshipId);
      });
      assertEquals(
          "No se puede eliminar la relacion patio-cliente porque tiene solicitudes de credito registradas",
          ex.getMessage());
    }

    @Test
    @DisplayName("When id is not null and client is found in database then delete relationship")
    public void returnAssignClientPatioDto() {
      //given
      Long relationshipId = 1L;
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(Optional.of(
          generateAssignClientPatio()));
      when(creditSolicitudeRepository.countByClient_IdAndPatio_IdAndStatusRegistered(anyLong(),
          anyLong()))
          .thenReturn(0L);
      //then
      clientPatioService.deleteClientPatioRelationship(relationshipId);
      verify(clientPatioRepository, times(1)).deleteById(relationshipId);
    }
  }

  @Nested
  @DisplayName("- Update Client-Patio relation")
  class UpdateClientPatioRelationship {

    public ClientPatioRelationship generateAssignClientPatio() {
      Client client = new Client();
      client.setId(1L);

      return ClientPatioRelationship.builder()
          .id(1L)
          .assignedAt(LocalDate.now())
          .patio(Patio.builder().id(1L).build())
          .client(client)
          .build();
    }

    public ClientPatioDto generateAssignClientPatioDtoClientIdNull() {
      return ClientPatioDto.builder()
          .id(1L)
          .assignedAt("2021-01-01")
          .clientDto(ClientDto.builder().id(null).build())
          .build();
    }

    public ClientPatioDto generateAssignClientPatioDtoPatioIdNull() {
      return ClientPatioDto.builder()
          .id(1L)
          .assignedAt("2021-01-01")
          .patioDto(PatioDto.builder().id(null).build())
          .clientDto(ClientDto.builder().id(1L).build())
          .build();
    }

    public ClientPatioDto generateValidAssignClientPatioDto(Long id) {
      return ClientPatioDto.builder()
          .id(id)
          .assignedAt("2021-01-01")
          .patioDto(PatioDto.builder().id(1L).build())
          .clientDto(ClientDto.builder().id(1L).build())
          .build();
    }

    @Test
    @DisplayName("When id is null then throw IllegalArgumentException")
    public void exceptionWhenIdNull() {
      //given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto(null);
      Long relationshipId = null;
      //when
      when(clientPatioRepository.findById(relationshipId)).thenThrow(
          new IllegalArgumentException("El id no puede ser nulo"));
      //then
      IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.updateClientPatioRelationship(clientPatioDto));
      assertEquals("El id no puede ser nulo", exception.getMessage());
    }

    @Test
    @DisplayName("When id is not in database then throw EntityNotFoundException")
    public void exceptionWhenIdNotFound() {
      //given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto(null);

      Long relationshipId = anyLong();
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(Optional.empty());
      //then
      Exception ex = assertThrows(EntityNotFoundException.class, () -> {
        clientPatioService.updateClientPatioRelationship(clientPatioDto);
      });
      assertEquals("La relacion patio cliente no existe", ex.getMessage());
    }


    @Test
    @DisplayName("When ClientDto.id is null then throw IllegalArgumentException")
    public void clientIdNull() {
      //Given
      ClientPatioDto clientPatioDto = generateAssignClientPatioDtoClientIdNull();
      //when
      when(clientPatioRepository.findById(anyLong())).thenReturn(
          Optional.of(new ClientPatioRelationship()));
      when(clientRepository.findById(null)).thenThrow(
          new RuntimeException("El id del cliente no puede ser nulo"));
      //then
      Exception ex = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.updateClientPatioRelationship(clientPatioDto));
      assertEquals("El id del cliente no puede ser nulo", ex.getMessage());
    }

    @Test
    @DisplayName("When PatioDto.id is null then throw IllegalArgumentException")
    public void patioIdNull() {
      //Given
      ClientPatioDto clientPatioDto = generateAssignClientPatioDtoPatioIdNull();
      //when
      when(clientPatioRepository.findById(anyLong())).thenReturn(
          Optional.of(new ClientPatioRelationship()));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
      when(patioRepository.findById(null)).thenThrow(
          new RuntimeException("El id del patio no puede ser nulo"));
      //then
      Exception ex = assertThrows(IllegalArgumentException.class,
          () -> clientPatioService.updateClientPatioRelationship(clientPatioDto));
      assertEquals("El id del patio no puede ser nulo", ex.getMessage());
    }

    @Test
    @DisplayName("When not client not found in database then return EntityNotFoundException")
    public void clientNotFound() {
      //Given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto(1L);
      //then
      when(clientPatioRepository.findById(anyLong())).thenReturn(
          Optional.of(new ClientPatioRelationship()));
      when(clientPatioRepository.save(any(ClientPatioRelationship.class))).thenReturn(
          getClientsPatioRelationship(5L));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.empty());
      //when
      Exception ex = assertThrows(EntityNotFoundException.class,
          () -> clientPatioService.updateClientPatioRelationship(clientPatioDto));
      assertEquals("El cliente no existe", ex.getMessage());
    }

    @Test
    @DisplayName("When not patio not found in database then return EntityNotFoundException")
    public void patioNotFound() {
      //Given
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto(null);
      //then
      when(clientPatioRepository.save(any(ClientPatioRelationship.class))).thenReturn(
          getClientsPatioRelationship(5L));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(
          new Client()));
      when(patioRepository.findById(anyLong())).thenReturn(
          Optional.empty());
      //when
      Exception ex = assertThrows(EntityNotFoundException.class,
          () -> clientPatioService.assignClientToPatio(clientPatioDto));
      assertEquals("El patio no existe", ex.getMessage());
    }


    @Test
    @DisplayName("When there is related data then throw ConflictException")
    public void exceptionWhenRelatedData() {
      //given
      Long relationshipId = anyLong();
      ClientPatioRelationship assignClientPatio = generateAssignClientPatio();
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(
          Optional.of(assignClientPatio));
      when(patioRepository.findById(anyLong())).thenReturn(Optional.of(
          Patio.builder().id(1L).build()));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
      when(creditSolicitudeRepository.countByClient_IdAndPatio_IdAndStatusRegistered(anyLong(),
          anyLong()))
          .thenReturn(1L);
      //then
      Exception ex = assertThrows(ConflictOperationException.class, () -> {
        clientPatioService.updateClientPatioRelationship(generateValidAssignClientPatioDto(1L));
      });
      assertEquals(
          "No se puede eliminar la relacion patio-cliente porque tiene solicitudes de credito registradas",
          ex.getMessage());
    }

    @Test
    public void updateClientPatioRelationship() {
      //Given
      Long relationshipId = 1L;
      ClientPatioDto clientPatioDto = generateValidAssignClientPatioDto(1L);
      //when
      when(clientPatioRepository.findById(relationshipId)).thenReturn(Optional.of(
          generateAssignClientPatio()));
      when(clientPatioRepository.save(any(ClientPatioRelationship.class))).thenReturn(
          getClientsPatioRelationship(5L));
      when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
      when(patioRepository.findById(anyLong())).thenReturn(
          Optional.of(Patio.builder().id(1L).build()));
      when(creditSolicitudeRepository.countByClient_IdAndPatio_IdAndStatusRegistered(anyLong(),
          anyLong()))
          .thenReturn(0L);
      //then
      clientPatioService.updateClientPatioRelationship(clientPatioDto);
      verify(clientPatioRepository, times(1)).save(any(ClientPatioRelationship.class));
    }

  }

}