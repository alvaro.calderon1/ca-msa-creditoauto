package com.bancopichincha.credito.automotriz.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.bancopichincha.credito.automotriz.domain.Client;
import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.repository.ClientPatioRepository;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.CreditSolicitudeRepository;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.CreditSolicitudeService;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import com.bancopichincha.credito.automotriz.service.dto.CreditSolicitudeDto;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.mapper.CreditSolicitudeMapper;
import com.bancopichincha.credito.automotriz.service.mapper.CreditSolicitudeMapperImpl;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

class CreditSolicitudeServiceImplTest {

  private final CreditSolicitudeRepository creditSolicitudeRepository;
  private final ClientPatioRepository clientPatioRepository;
  private final CreditSolicitudeService creditSolicitudeService;
  private final VehicleRepository vehicleRepository;
  private final ExecutiveRepository executiveRepository;
  private final PatioRepository patioRepository;
  private final ClientRepository clientRepository;
  private final CreditSolicitudeMapper mapper;

  public CreditSolicitudeServiceImplTest() {
    creditSolicitudeRepository = mock(CreditSolicitudeRepository.class);
    clientPatioRepository = mock(ClientPatioRepository.class);
    vehicleRepository = mock(VehicleRepository.class);
    executiveRepository = mock(ExecutiveRepository.class);
    patioRepository = mock(PatioRepository.class);
    clientRepository = mock(ClientRepository.class);
    mapper = new CreditSolicitudeMapperImpl();
    creditSolicitudeService = new CreditSolicitudeServiceImpl(creditSolicitudeRepository,
        clientPatioRepository, vehicleRepository, executiveRepository, patioRepository,
        clientRepository, mapper);
  }

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }


  @Test
  @DisplayName("Given a credit solicitude already created today, when createCreditSolicitude, then return ConflictException")
  void givenACreditSolicitudeAlreadyCreatedToday_whenCreateCreditSolicitude_thenReturnConflictException() {
    // given
    CreditSolicitudeDto creditSolicitudeDto = getCreditSolicitudeDto();
    // when
    when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
    when(patioRepository.findById(anyLong())).thenReturn(Optional.of(new Patio()));
    when(executiveRepository.findById(anyLong())).thenReturn(Optional.of(new Executive()));
    when(vehicleRepository.findById(anyLong())).thenReturn(Optional.of(new Vehicle()));
    when(creditSolicitudeRepository.save(any())).thenReturn(getCreditSolicitudeDto());
    when(creditSolicitudeRepository.countTodayActiveSolicitudes(anyLong(),
        any(LocalDate.class))).thenReturn(1L);
    // then
    Exception ex = assertThrows(ConflictOperationException.class,
        () -> creditSolicitudeService.createCreditSolicitude(creditSolicitudeDto));
    assertEquals("Ya existe una solicitud de crédito activa para el cliente el dia de hoy",
        ex.getMessage());
  }

  @Test
  @DisplayName("Given a credit solicitude created that has a vehicle already used in another solicitude, when createCreditSolicitude, then return ConflictException")
  void givenACreditSolicitudeCreatedThatHasAVehicleAlreadyUsedInAnotherSolicitude_whenCreateCreditSolicitude_thenReturnConflictException() {
    // given
    CreditSolicitudeDto creditSolicitudeDto = getCreditSolicitudeDto();
    // when
    when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
    when(patioRepository.findById(anyLong())).thenReturn(Optional.of(new Patio()));
    when(executiveRepository.findById(anyLong())).thenReturn(Optional.of(new Executive()));
    when(vehicleRepository.findById(anyLong())).thenReturn(Optional.of(new Vehicle()));
    when(creditSolicitudeRepository.countTodayActiveSolicitudes(anyLong(),
        any(LocalDate.class))).thenReturn(0L);
    when(creditSolicitudeRepository.existsByVehicle_IdAndActiveSolicitude(anyLong())).thenReturn(
        true);
    // then
    Exception ex = assertThrows(ConflictOperationException.class,
        () -> creditSolicitudeService.createCreditSolicitude(creditSolicitudeDto));
    assertEquals("El auto que envio para la solicitud de credito ya esta reservado para otro",
        ex.getMessage());
  }

  @Test
  @DisplayName(
      "Given a credit solicitude created that has a patio and client that don't have a relation, "
          + "when createCreditSolicitude, "
          + "then create relation between them")
  void givenACreditSolicitudeCreatedThatHasAPatioAndClientThatDontHaveARelation_whenCreateCreditSolicitude_thenCreateRelationBetweenThem() {
    // given
    CreditSolicitudeDto creditSolicitudeDto = getCreditSolicitudeDto();
    // when
    when(clientRepository.findById(anyLong())).thenReturn(Optional.of(new Client()));
    when(patioRepository.findById(anyLong())).thenReturn(Optional.of(new Patio()));
    when(executiveRepository.findById(anyLong())).thenReturn(Optional.of(new Executive()));
    when(vehicleRepository.findById(anyLong())).thenReturn(Optional.of(new Vehicle()));
    when(clientPatioRepository.existsByClient_IdAndPatio_Id(anyLong(), anyLong())).thenReturn(
        false);
    when(creditSolicitudeRepository.countTodayActiveSolicitudes(anyLong(),
        any(LocalDate.class))).thenReturn(0L);
    when(creditSolicitudeRepository.existsByVehicle_IdAndActiveSolicitude(anyLong())).thenReturn(
        false);
    // then
    creditSolicitudeService.createCreditSolicitude(creditSolicitudeDto);
    when(clientPatioRepository.existsByClient_IdAndPatio_Id(anyLong(), anyLong())).thenReturn(
        true);
    assertTrue(clientPatioRepository.existsByClient_IdAndPatio_Id(anyLong(), anyLong()));
  }


  private ClientDto getClientDto() {
    return ClientDto.builder()
        .id(1L)
        .name("name")
        .lastName("lastName")
        .phone("phone")
        .build();
  }

  private PatioDto getPatioDto() {
    return PatioDto.builder()
        .id(1L)
        .name("name")
        .build();
  }

  private ExecutiveDto getExecutiveDto() {
    return ExecutiveDto.builder()
        .id(1L)
        .name("name")
        .lastName("lastName")
        .build();
  }

  private VehicleDto getVehicleDto() {
    return VehicleDto.builder()
        .id(1L)
        .brand(new BrandDto(1L, "brand"))
        .model("model")
        .build();
  }

  private CreditSolicitudeDto getCreditSolicitudeDto() {
    return CreditSolicitudeDto.builder()
        .id(1L)
        .client(getClientDto())
        .patio(getPatioDto())
        .executive(getExecutiveDto())
        .vehicle(getVehicleDto())
        .date("2020-01-01")
        .quotas(1)
        .entry(1L)
        .months(1)
        .status("R")
        .build();
  }

}