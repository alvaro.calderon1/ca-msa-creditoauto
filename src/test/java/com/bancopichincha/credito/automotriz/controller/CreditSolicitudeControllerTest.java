package com.bancopichincha.credito.automotriz.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bancopichincha.credito.automotriz.CaMsaCreditoautoApplication;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.service.CreditSolicitudeService;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import com.bancopichincha.credito.automotriz.service.dto.CreditSolicitudeDto;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CaMsaCreditoautoApplication.class})
@WebAppConfiguration
class CreditSolicitudeControllerTest {

  private final WebApplicationContext webApplicationContext;
  private final ObjectMapper mapper;

  @MockBean
  private CreditSolicitudeService creditSolicitudeService;

  private MockMvc mockMvc;
  private String message;

  public CreditSolicitudeControllerTest(WebApplicationContext webApplicationContext) {
    this.webApplicationContext = webApplicationContext;
    this.mapper = new ObjectMapper();
  }

  @BeforeEach
  public void setup() throws Exception {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
        .addFilter(((request, response, chain) -> {
          response.setCharacterEncoding("UTF-8");
          chain.doFilter(request, response);
        }))
        .build();
  }

  @Test
  @DisplayName("Test createCreditSolicitude with invalid data")
  public void sendingNullInMandatoryParameters() throws Exception {
    String[] expected = {"cliente", "patio", "ejecutivo", "vehiculo", "fecha de elaboracion",
        "plazo", "cuotas", "entrada", "estado"};
    MvcResult mvcResult = this.mockMvc.perform(
            post("/v1/api/solicitudCredito/crear")
                .contentType("application/json")
                .content("{\"id\":1}")).andDo(print())
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.hasErrors").value(true))
        .andExpect(jsonPath("$.error.errors").isArray())
        .andReturn();
    String content = mvcResult.getResponse().getContentAsString().toLowerCase();
    for (String s : expected) {

      assertTrue(content.contains(s.toLowerCase()));
    }
    assertEquals("application/json", mvcResult.getResponse().getContentType());
  }

  @Test
  @DisplayName("Test createCreditSolicitude when there is already a credit for that user in that day")
  public void sendingTwoSolicitudedInOneDay() throws Exception {
    when(creditSolicitudeService.createCreditSolicitude(any(CreditSolicitudeDto.class)))
        .thenThrow(
            new ConflictOperationException(
                "Ya existe una solicitud de crédito activa para el cliente"));
    MvcResult mvcResult = this.mockMvc.perform(
            post("/v1/api/solicitudCredito/crear")
                .contentType("application/json")
                .content(mapper.writeValueAsString(getCreditSolicitudeDto())))
        .andDo(print())
        .andExpect(status().isConflict())
        .andExpect(jsonPath("$.hasErrors").value(true))
        .andExpect(jsonPath("$.error").exists())
        .andExpect(jsonPath("$.error.errors").isArray())
        .andReturn();
  }

  @Test
  @DisplayName("Test createCreditSolicitude when a vehicle is already register in a solicitude")
  public void sendingASolicitudeWithAnAssignedVehicle() throws Exception {
    message = "El auto que envio para la solicitud de credito ya esta reservado para otro";
    when(creditSolicitudeService.createCreditSolicitude(any(CreditSolicitudeDto.class))).thenThrow(
        new ConflictOperationException(
            message));
    MvcResult mvcResult = this.mockMvc.perform(
            post("/v1/api/solicitudCredito/crear")
                .contentType("application/json")
                .content(mapper.writeValueAsString(getCreditSolicitudeDto())))
        .andDo(print())
        .andExpect(status().isConflict())
        .andExpect(jsonPath("$.hasErrors").value(true))
        .andExpect(jsonPath("$.error").exists())
        .andExpect(jsonPath("$.error.errors").isArray())
        .andExpect(jsonPath("$.error.errors[0]").value(
            message))
        .andReturn();
  }


  @Test
  @DisplayName("Test createCreditSolicitude with valid data")
  public void sendingValidData() throws Exception {
    MvcResult mvcResult = this.mockMvc.perform(
            post("/v1/api/solicitudCredito/crear")
                .characterEncoding("utf-8")
                .contentType("application/json")
                .content(mapper.writeValueAsString(getCreditSolicitudeDto())))
        .andDo(print())
        .andExpect(status().isCreated())
        .andReturn();
  }

  private ClientDto getClientDto() {
    return ClientDto.builder()
        .id(1L)
        .name("name")
        .lastName("lastName")
        .phone("phone")
        .build();
  }

  private PatioDto getPatioDto() {
    return PatioDto.builder()
        .id(1L)
        .name("name")
        .build();
  }

  private ExecutiveDto getExecutiveDto() {
    return ExecutiveDto.builder()
        .id(1L)
        .name("name")
        .lastName("lastName")
        .build();
  }

  private VehicleDto getVehicleDto() {
    return VehicleDto.builder()
        .id(1L)
        .brand(new BrandDto(1L, "brand"))
        .model("model")
        .build();
  }

  private CreditSolicitudeDto getCreditSolicitudeDto() {
    return CreditSolicitudeDto.builder()
        .id(1L)
        .client(getClientDto())
        .patio(getPatioDto())
        .executive(getExecutiveDto())
        .vehicle(getVehicleDto())
        .date("2020-01-01")
        .quotas(1)
        .entry(1L)
        .months(1)
        .status("status")
        .build();
  }

}


