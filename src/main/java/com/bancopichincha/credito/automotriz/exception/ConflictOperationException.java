package com.bancopichincha.credito.automotriz.exception;

public class ConflictOperationException extends RuntimeException {
  
    public ConflictOperationException(String message) {
        super(message);
    }

}
