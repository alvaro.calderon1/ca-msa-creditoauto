package com.bancopichincha.credito.automotriz.configuration;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.Client;
import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.emuns.MaritalStatus;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.repository.PersonRepository;
import com.bancopichincha.credito.automotriz.util.FileUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Log4j2
public class DataLoader implements ApplicationRunner {

  private final ClientRepository clientRepository;

  private final PersonRepository personRepository;

  private final PatioRepository patioRepository;

  private final BrandRepository brandRepository;

  @Override
  public void run(ApplicationArguments args) throws Exception {
    loadPatios();
    log.info("Loading initial data");
    log.info("Loading clients");
    loadClients();
    log.info("Loading executives");
    loadExecutives();
    log.info("Loading brands");
    loadBrands();
    log.info("Initial data loaded");
  }

  private void loadPatios() {
    List<String> lines;
    try {
      lines = FileUtils.getResourceFileLines("initialData/patio.csv");
    } catch (IOException e) {
      log.error("Error loading patios", e);
      return;
    }
    lines.remove(0);
    lines.forEach(line -> {
      String[] columns = line.split(",");
      Long id = Long.parseLong(columns[0].trim());
      String name = columns[1].trim();
      String address = columns[2].trim();
      String phone = columns[3].trim();
      Integer numberPos = Integer.valueOf(columns[4].trim());
      if (patioRepository.existsByNameIgnoreCase(name)) {
        log.error("Patio {} already exists", name);
        return;
      }
      patioRepository.save(new Patio(id, name, address, phone, numberPos));
    });
  }

  private void loadBrands() throws IOException {
    List<String> lines;
    try {
      lines = FileUtils.getResourceFileLines("initialData/brands.csv");
    } catch (IOException e) {
      log.error("Error loading brands", e);
      return;
    }
    lines.remove(0);
    lines.forEach((line) -> {
          String[] columns = line.split(",");
          Long id = Long.parseLong(columns[0].trim());
          String name = columns[1].trim();
          if (brandRepository.existsByNameIgnoreCase(name)) {
            log.error("Brand {} already exists", name);
            return;
          }
          brandRepository.save(new Brand(id, name));
        }
    );
  }

  private void loadExecutives() {
    List<String> lines;
    try {
      lines = FileUtils.getResourceFileLines("initialData/executives.csv");
    } catch (IOException e) {
      log.error("Error loading brands", e);
      return;
    }
    lines.remove(0);
    lines.forEach(line -> {
      String identification, name, lastname, address, phone, cellphone;
      Long id, patioId;
      Integer age;
      String[] columns = line.split(",");
      id = Long.parseLong(columns[0].trim());
      identification = columns[1].trim();
      name = columns[2].trim();
      lastname = columns[3].trim();
      address = columns[4].trim();
      phone = columns[5].trim();
      cellphone = columns[6].trim();
      patioId = Long.parseLong(columns[7].trim());
      age = Integer.parseInt(columns[8].trim());
      if (personRepository.existsByIdentificationIgnoreCase(identification)) {
        log.error("Executive {} already exists", identification);
        return;
      }
      Optional<Patio> patio = patioRepository.findById(patioId);
      if (patio.isEmpty()) {
        log.error("Patio {} does not exist", patioId);
        return;
      }
      Executive executive = new Executive(id, identification, name, lastname, address, phone,
          cellphone, age, patio.get());
      personRepository.save(executive);
    });
  }

  private void loadClients() {
    List<String> lines;
    try {
      lines = FileUtils.getResourceFileLines("initialData/client.csv");
    } catch (IOException e) {
      log.error("Error loading brands", e);
      return;
    }
    lines.remove(0);
    lines.forEach(line -> {
      long id;
      int age, maritalStatus;
      MaritalStatus enumMaritalStatus;
      String[] columns;
      String identification, name, lastname, birthdate, address, phone,
          spouseIdentification, spouseName, subjectToCredit;
      columns = line.split(",");
      id = Long.parseLong(columns[0].trim());
      identification = columns[1].trim();
      name = columns[2].trim();
      lastname = columns[3].trim();
      birthdate = columns[4].trim();
      address = columns[5].trim();
      phone = columns[6].trim();
      maritalStatus = Integer.parseInt(columns[7].trim());
      spouseIdentification = columns[8].trim();
      spouseName = columns[9].trim();
      subjectToCredit = columns[10].trim();
      if (personRepository.existsByIdentificationIgnoreCase(identification)) {
        log.error("Client {} already exists", identification);
        return;
      }
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
      LocalDate parsedBirthdate = LocalDate.parse(birthdate, formatter);
      age = LocalDate.now().getYear() - parsedBirthdate.getYear();
      enumMaritalStatus = MaritalStatus.values()[maritalStatus];

      clientRepository.save(new Client(id, identification, name, lastname, address,
          phone, null, age, enumMaritalStatus, spouseIdentification, spouseName,
          Objects.equals(subjectToCredit, "Y"),
          parsedBirthdate));
    });

  }
}
