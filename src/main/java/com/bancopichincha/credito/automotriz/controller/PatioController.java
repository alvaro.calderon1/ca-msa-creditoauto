package com.bancopichincha.credito.automotriz.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1/api/patio")
@RestController
@RequiredArgsConstructor
public class PatioController {

  private final PatioService patioService;

  @GetMapping("/buscar")
  @ResponseStatus(OK)
  public PatioDto getPatio(@RequestParam(value = "id") Long id) {
    return patioService.getPatio(id);
  }

  @PostMapping("/crear")
  @ResponseStatus(CREATED)
  public PatioDto createPatio(@Valid @RequestBody PatioDto patioDto) {
    return patioService.createPatio(patioDto);
  }

  @PutMapping("/actualizar")
  @ResponseStatus(OK)
  public PatioDto updatePatio(@Valid @RequestBody PatioDto patioDto) {
    return patioService.updatePatio(patioDto);
  }

  @DeleteMapping("/eliminar")
  @ResponseStatus(OK)
  public boolean deletePatio(@RequestParam(value = "id") Long id) {
    patioService.deletePatio(id);
    return true;
  }
}
