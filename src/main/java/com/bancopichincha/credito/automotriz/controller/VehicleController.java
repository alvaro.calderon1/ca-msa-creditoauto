package com.bancopichincha.credito.automotriz.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/api/vehiculo")
@RequiredArgsConstructor
public class VehicleController {

  private final VehicleService vehicleService;

  @GetMapping("/buscar")
  @ResponseStatus(OK)
  public List<VehicleDto> getVehicleFilters(@RequestParam Map<String, String> filters) {
    return vehicleService.getVehiclesbyFilters(filters);
  }

  @PostMapping("/crear")
  @ResponseStatus(CREATED)
  public VehicleDto saveVehicle(@Valid @RequestBody VehicleDto vehicleDto) {
    return vehicleService.createVehicle(vehicleDto);
  }

  @PutMapping("/actualizar")
  @ResponseStatus(OK)
  public VehicleDto updateVehicle(@Valid @RequestBody VehicleDto vehicleDto) {
    return vehicleService.updateVehicle(vehicleDto);
  }

  @PatchMapping("/actualizarEstado")
  @ResponseStatus(OK)
  public VehicleDto updateVehiclePatch(@RequestBody VehicleDto vehicleDto) {
    return vehicleService.updateStatus(vehicleDto);
  }

  @DeleteMapping("/eliminar")
  @ResponseStatus(OK)
  public boolean deleteVehicle(@RequestParam Long id) {
    vehicleService.deleteVehicle(id);
    return true;
  }
}
