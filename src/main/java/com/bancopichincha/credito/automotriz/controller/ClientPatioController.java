package com.bancopichincha.credito.automotriz.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.bancopichincha.credito.automotriz.service.ClientPatioService;
import com.bancopichincha.credito.automotriz.service.dto.ClientPatioDto;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1/api/clientePatio")
@RestController
@RequiredArgsConstructor
public class ClientPatioController {

  private final ClientPatioService clientPatioService;


  @GetMapping("/buscar")
  @ResponseStatus(OK)
  public ClientPatioDto getClientsPatioRelationship(@RequestParam("id") Long idRelationship) {
    return clientPatioService.getClientsPatioRelationship(idRelationship);
  }

  @PostMapping("/crear")
  @ResponseStatus(CREATED)
  public ClientPatioDto crearClientePatio(
      @Valid @RequestBody ClientPatioDto clientePatio) {
    return clientPatioService.assignClientToPatio(clientePatio);
  }

  @DeleteMapping("/eliminar")
  @ResponseStatus(OK)
  public boolean deleteClientPatioRelationship(@RequestParam("id") Long idRelationship) {
    clientPatioService.deleteClientPatioRelationship(idRelationship);
    return true;
  }

  @PutMapping("/actualizar")
  @ResponseStatus(OK)
  public ClientPatioDto updateClientPatioRelationship(
      @Valid @RequestBody ClientPatioDto clientPatio) {
    return clientPatioService.updateClientPatioRelationship(clientPatio);
  }
}
