package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.service.dto.ApiDto;
import com.bancopichincha.credito.automotriz.service.dto.ApiErrorDto;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorHandlerController extends ResponseEntityExceptionHandler {

  private ResponseEntity<Object> buildResponseEntity(ApiDto<ApiErrorDto> apiDto) {
    return new ResponseEntity<>(apiDto, apiDto.getError().getStatus());
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    ApiDto<ApiErrorDto> response = new ApiDto<>(
        new ApiErrorDto(HttpStatus.BAD_REQUEST, errorBody("Malformed JSON request")));
    return buildResponseEntity(response);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ResponseEntity<Object> handleUserNotFoundException(EntityNotFoundException ex) {
    ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.NOT_FOUND, errorBody(ex.getMessage()));
    ApiDto<ApiErrorDto> response = new ApiDto<>(apiErrorDto);
    return buildResponseEntity(response);
  }

  @ExceptionHandler(value = {IllegalArgumentException.class,
      JdbcSQLIntegrityConstraintViolationException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ResponseEntity<Object> handleIllegalArgumentException(Exception ex) {
    ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.BAD_REQUEST, errorBody(ex.getMessage()));
    ApiDto<ApiErrorDto> response = new ApiDto<>(apiErrorDto);
    return buildResponseEntity(response);
  }


  @ExceptionHandler(value = {ConflictOperationException.class})
  @ResponseStatus(HttpStatus.CONFLICT)
  @ResponseBody
  public ResponseEntity<Object> handleConflictOperationException(Exception ex) {
    ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.CONFLICT, errorBody(ex.getMessage()));
    ApiDto<ApiErrorDto> response = new ApiDto<>(apiErrorDto);
    return buildResponseEntity(response);
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    ApiDto<ApiErrorDto> response = new ApiDto<>(new ApiErrorDto(HttpStatus.BAD_REQUEST,
        errorBody(" No se ha enviado el parametro: " + ex.getParameterName())));
    return buildResponseEntity(response);
  }

  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    ApiDto<ApiErrorDto> response = new ApiDto<>(
        new ApiErrorDto(HttpStatus.NOT_FOUND, errorBody(ex.getMessage())));
    return buildResponseEntity(response);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    var exceptionErrors = ex.getBindingResult().getFieldErrors().stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.toList());
    ApiDto<ApiErrorDto> response = new ApiDto<>(
        new ApiErrorDto(HttpStatus.BAD_REQUEST, errorBody(exceptionErrors)));
    return buildResponseEntity(response);
  }

//  @ExceptionHandler(Exception.class)
//  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//  @ResponseBody
//  public ResponseEntity<Object> handleException(Exception ex) {
//    ApiErrorDto apiErrorDto = new ApiErrorDto(HttpStatus.INTERNAL_SERVER_ERROR,
//        errorBody(ex.getMessage()));
//    ApiDto<ApiErrorDto> response = new ApiDto<>(apiErrorDto);
//    return buildResponseEntity(response);
//  }

  private List<String> errorBody(String error) {
    List<String> errors = new ArrayList<>();
    errors.add(error);
    return errors;
  }

  private List<String> errorBody(List<String> exceptionErrors) {
    return exceptionErrors;
  }
}