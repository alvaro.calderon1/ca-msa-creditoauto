package com.bancopichincha.credito.automotriz.controller;

import static org.springframework.http.HttpStatus.CREATED;

import com.bancopichincha.credito.automotriz.service.CreditSolicitudeService;
import com.bancopichincha.credito.automotriz.service.dto.CreditSolicitudeDto;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1/api/solicitudCredito")
@RestController
@RequiredArgsConstructor
public class CreditSolicitudeController {

  private final CreditSolicitudeService creditSolicitudeService;

  @PostMapping(value = "/crear")
  @ResponseStatus(CREATED)
  public CreditSolicitudeDto createCreditSolicitude(
      @Valid @RequestBody CreditSolicitudeDto creditSolicitudeDto) {
    return creditSolicitudeService.createCreditSolicitude(creditSolicitudeDto);
  }

}
