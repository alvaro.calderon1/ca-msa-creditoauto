package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.repository.VehicleRepositoryCustom;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@Log4j2
public class VehicleRepositoryCustomImpl implements VehicleRepositoryCustom {

  @PersistenceContext
  private final EntityManager entityManager;

  @Override
  public List<Vehicle> getVehiclesbyFilters(Map<String, String> filters) {
    String queryString = generateQuery(filters);
    int counter = 1;
    log.info("Query: {}", queryString);
    TypedQuery<Vehicle> query = entityManager.createQuery(queryString, Vehicle.class);
    for (Map.Entry<String, String> entry : filters.entrySet()) {
      switch (entry.getKey()) {
        case "idMarca":
          query.setParameter(counter++, Long.parseLong(entry.getValue()));
          break;
        case "modelo":
        case "ano":
          query.setParameter(counter++, entry.getValue());
          break;
      }
    }
    return query.getResultList();
  }

  private String generateQuery(Map<String, String> filters) {
    StringBuilder filter = new StringBuilder("SELECT v FROM Vehicle v WHERE");
    int counter = 1;
    for (Map.Entry<String, String> entry : filters.entrySet()) {
      switch (entry.getKey()) {
        case "idMarca":
          filter.append(" v.brand.id = ?").append(counter++).append(" AND");
          break;
        case "modelo":
          filter.append(" v.model = ?").append(counter++).append(" AND");
          break;
        case "ano":
          filter.append(" v.fabricationDate = ?").append(counter++).append(" AND");
          break;
        default:
          throw new IllegalArgumentException("Invalid filter: " + entry.getKey() + " not sported.");
      }
    }
    if (filters.size() > 0) {
      filter = new StringBuilder(filter.substring(0, filter.length() - 4));
    } else {
      filter = new StringBuilder(filter.substring(0, filter.length() - 6));
    }
    return filter.toString();
  }
}
