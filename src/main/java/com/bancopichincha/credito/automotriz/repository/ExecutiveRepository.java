package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Executive;
import org.springframework.data.repository.CrudRepository;

public interface ExecutiveRepository extends CrudRepository<Executive, Long> {

}