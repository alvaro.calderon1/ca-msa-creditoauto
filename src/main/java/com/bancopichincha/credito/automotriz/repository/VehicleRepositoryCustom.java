package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import java.util.List;
import java.util.Map;

public interface VehicleRepositoryCustom {

  List<Vehicle> getVehiclesbyFilters(Map<String, String> filters);
}
