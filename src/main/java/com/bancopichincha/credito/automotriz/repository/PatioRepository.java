package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Patio;
import org.springframework.data.repository.CrudRepository;

public interface PatioRepository extends CrudRepository<Patio, Long> {

  boolean existsByNameIgnoreCase(String name);
}
