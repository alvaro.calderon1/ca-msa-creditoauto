package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.ClientPatioRelationship;
import org.springframework.data.repository.CrudRepository;

public interface ClientPatioRepository extends CrudRepository<ClientPatioRelationship, Long> {

  boolean existsByClient_IdAndPatio_Id(Long clientId, Long patioId);
}
