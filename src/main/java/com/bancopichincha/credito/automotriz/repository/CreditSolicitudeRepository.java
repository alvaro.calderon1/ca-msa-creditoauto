package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CreditSolicitude;
import java.time.LocalDate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CreditSolicitudeRepository extends CrudRepository<CreditSolicitude, Long> {

  @Query("select count(c) from CreditSolicitude c "
      + "where c.client.id = ?1 and c.patio.id = ?2 "
      + "and c.status = com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus.REGISTERED")
  long countByClient_IdAndPatio_IdAndStatusRegistered(
      Long clientId, Long patioId);

  @Query(
      "select count(c) from CreditSolicitude c where c.client.id = ?1 and c.createdAt = ?2 and c.status in ("
          + "com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus.REGISTERED, "
          + "com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus.DISPATCHED) ")
  long countTodayActiveSolicitudes(Long clientId, LocalDate date);

  @Query("select (count(c) > 0) from CreditSolicitude c where c.vehicle.id = ?1 and c.status  "
      + "in (com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus.REGISTERED, "
      + "com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus.DISPATCHED)")
  boolean existsByVehicle_IdAndActiveSolicitude(Long vehicleId);

}