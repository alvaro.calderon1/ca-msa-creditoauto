package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

public interface VehicleRepository extends CrudRepository<Vehicle, Long>, VehicleRepositoryCustom {

  boolean existsByPlateIgnoreCase(@NonNull String plate);

  Optional<Vehicle> findByPlateIgnoreCase(@NonNull String plate);

}
