package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.emuns.MaritalStatus;
import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.Client} entity
 */
@Data
@Builder
public class ClientDto implements Serializable {

  private final Long id;
  private final String identification;
  private final String name;
  private final String lastName;
  private final String address;
  private final String phone;
  private final String cellphone;
  private final Integer age;
  private final MaritalStatus maritalStatus;
  private final String spouseIdentification;
  private final String spouseName;
  private final Boolean isSubjectToCredit;
  private final String birthDate;
}