package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = ComponentModel.SPRING)
public interface PatioMapper {


  Patio toEntity(PatioDto patioDto);

  PatioDto toDto(Patio patio);
}