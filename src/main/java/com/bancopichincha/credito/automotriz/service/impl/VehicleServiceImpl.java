package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.domain.emuns.VehicleStatus;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

  private final VehicleRepository vehicleRepository;
  private final BrandRepository brandRepository;
  private final VehicleMapper vehicleMapper;

  @Override
  public List<VehicleDto> getVehiclesbyFilters(Map<String, String> filters) {
    List<Vehicle> vehicles;
    vehicles = vehicleRepository.getVehiclesbyFilters(filters);
    return vehicles.stream().map(vehicleMapper::toDto).collect(Collectors.toList());
  }

  @Override
  public VehicleDto createVehicle(VehicleDto vehicleDto) {
    Vehicle vehicle, savedVehicle;
    Brand brand;
    validatePlate(vehicleDto.getPlate());
    brand = getBrandIfExists(vehicleDto.getBrand().getId());
    vehicle = vehicleMapper.toEntity(vehicleDto);
    vehicle.setBrand(brand);
    mapStatus(vehicleDto, vehicle);
    vehicle.setStatus(VehicleStatus.REGISTERED);
    savedVehicle = vehicleRepository.save(vehicle);
    return vehicleMapper.toDto(savedVehicle);
  }

  private Brand getBrandIfExists(Long id) {
    Optional<Brand> brand = brandRepository.findById(id);
    return brand.orElseThrow(() -> new EntityNotFoundException("La marca no existe"));
  }

  private void validatePlate(String plate) {
    boolean exists = vehicleRepository.existsByPlateIgnoreCase(plate);
    if (exists) {
      throw new ConflictOperationException("Ya existe un vehiculo con Placa: " + plate);
    }
  }

  private Vehicle getVehicleIfExists(Long id) {
    Optional<Vehicle> patio;
    try {
      patio = vehicleRepository.findById(id);
    } catch (Exception e) {
      throw new IllegalArgumentException("El id no puede ser nulo");
    }
    return patio.orElseThrow(() -> new EntityNotFoundException("El patio no existe"));
  }

  @Override
  public VehicleDto updateVehicle(VehicleDto vehicleDto) {
    Vehicle vehicle, savedVehicle, fetchedVehicle;
    VehicleStatus newStatus;
    fetchedVehicle = getVehicleIfExists(vehicleDto.getId());
    vehicle = vehicleMapper.toEntity(vehicleDto);
    return updateValidateStatus(vehicleDto, vehicle,
        fetchedVehicle.isStatus(VehicleStatus.IN_CREDIT));
  }

  private VehicleStatus mapStatus(VehicleDto vehicleDto, Vehicle vehicle) {
    String status = vehicleDto.getStatus();
    if (status == null) {
      return vehicle.getStatus();
    }
    switch (status) {
      case "V":
        return VehicleStatus.SOLD;
      case "R":
        return VehicleStatus.REGISTERED;
      case "P":
        return VehicleStatus.IN_PARKING;
      case "C":
        return VehicleStatus.IN_CREDIT;
      default:
        throw new IllegalArgumentException("Invalid status: " + status + " not sported.");
    }
  }

  @Override
  public void deleteVehicle(Long id) {
    Vehicle vehicle = getVehicleIfExists(id);
    if (vehicle.getStatus().equals(VehicleStatus.IN_CREDIT)) {
      throw new ConflictOperationException(
          "Existe una solicitud de credito asociada al vehiculo que desea eliminar");
    }
    vehicleRepository.delete(vehicle);
  }

  @Override
  public VehicleDto updateStatus(VehicleDto vehicleDto) {
    Vehicle vehicle, savedVehicle;
    VehicleStatus newStatus;
    vehicle = getVehicleIfExists(vehicleDto.getId());
    if (vehicleDto.getStatus() == null) {
      throw new IllegalArgumentException("El estado no puede ser nulo");
    }
    return updateValidateStatus(vehicleDto, vehicle, vehicle.isStatus(VehicleStatus.IN_CREDIT));
  }

  private VehicleDto updateValidateStatus(VehicleDto vehicleDto, Vehicle vehicle, boolean status) {
    VehicleStatus newStatus;
    Vehicle savedVehicle;
    newStatus = mapStatus(vehicleDto, vehicle);
    if (status && newStatus.equals(VehicleStatus.SOLD)) {
      throw new ConflictOperationException(
          "No se puede vender el vehiculo porque ya está reservada para un credito");
    }
    vehicle.setStatus(newStatus);
    savedVehicle = vehicleRepository.save(vehicle);
    return vehicleMapper.toDto(savedVehicle);
  }

}
