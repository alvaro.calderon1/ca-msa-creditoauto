package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import java.util.List;
import java.util.Map;

public interface VehicleService {

  List<VehicleDto> getVehiclesbyFilters(Map<String, String> filtes);

  VehicleDto createVehicle(VehicleDto vehicleDto);

  VehicleDto updateVehicle(VehicleDto vehicleDto);

  void deleteVehicle(Long id);

  VehicleDto updateStatus(VehicleDto vehicleDto);
}
