package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.PatioDto;

public interface PatioService {

  PatioDto createPatio(PatioDto patioDto);

  PatioDto updatePatio(PatioDto patioDto);

  void deletePatio(Long id);

  PatioDto getPatio(Long id);
}
