package com.bancopichincha.credito.automotriz.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiDto<T> {

  private T data;
  private boolean hasErrors;
  private ApiErrorDto error;

  public ApiDto(ApiErrorDto error) {
    this.hasErrors = true;
    this.error = error;
  }

  public ApiDto(T data) {
    this.data = data;
    this.hasErrors = false;
  }
}