package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.Executive} entity
 */
@Data
@Builder
public class ExecutiveDto implements Serializable {

  private final Long id;
  private final String identification;
  private final String name;
  private final String lastName;
  private final String address;
  private final String phone;
  private final String cellphone;
  private final Integer age;
  private final PatioDto patioDto;
}