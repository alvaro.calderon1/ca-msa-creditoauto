package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.Client;
import com.bancopichincha.credito.automotriz.domain.ClientPatioRelationship;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.repository.ClientPatioRepository;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.CreditSolicitudeRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.service.ClientPatioService;
import com.bancopichincha.credito.automotriz.service.dto.ClientPatioDto;
import com.bancopichincha.credito.automotriz.service.mapper.ClientPatioRelationshipMapper;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Log4j2
public class ClientPatioServiceImpl implements ClientPatioService {

  private final ClientPatioRepository clientPatioRepository;
  private final ClientRepository clientRepository;
  private final ClientPatioRelationshipMapper clientPatioMapper;
  private final CreditSolicitudeRepository creditSolicitudeRepository;
  private final PatioRepository patioRepository;

  @Override
  public ClientPatioDto getClientsPatioRelationship(Long idRelationship) {
    ClientPatioRelationship relationship = getRelationshipIfExist(idRelationship);
    return clientPatioMapper.toDto(relationship);
  }

  private ClientPatioRelationship getRelationshipIfExist(Long idRelationship) {
    Optional<ClientPatioRelationship> clientPatioRelationship;
    try {
      clientPatioRelationship = clientPatioRepository.findById(idRelationship);
    } catch (Exception e) {
      throw new IllegalArgumentException("El id no puede ser nulo");
    }
    return clientPatioRelationship.orElseThrow(
        () -> new EntityNotFoundException("La relacion patio cliente no existe"));
  }


  @Override
  public ClientPatioDto assignClientToPatio(@Valid ClientPatioDto clientPatio) {
    getClientIfExists(clientPatio);
    getPatioIfExists(clientPatio);
    validateAssignationExistance(clientPatio);
    ClientPatioRelationship clientPatioRelationship = clientPatioMapper.toEntity(clientPatio);
    clientPatioRelationship = clientPatioRepository.save(clientPatioRelationship);
    return clientPatioMapper.toDto(clientPatioRelationship);
  }

  private void validateAssignationExistance(ClientPatioDto clientPatio) {
    if (clientPatioRepository.existsByClient_IdAndPatio_Id(clientPatio.getClientDto().getId(),
        clientPatio.getPatioDto().getId())) {
      throw new ConflictOperationException("El cliente ya esta asignado a ese patio");
    }
  }

  public Patio getPatioIfExists(ClientPatioDto clientPatio) {
    Optional<Patio> patio;
    try {
      patio = patioRepository.findById(clientPatio.getPatioDto().getId());
    } catch (Exception e) {
      throw new IllegalArgumentException("El id del patio no puede ser nulo");
    }
    return patio.orElseThrow(() -> new EntityNotFoundException("El patio no existe"));
  }


  private Client getClientIfExists(ClientPatioDto clientPatio) {
    Optional<Client> client;
    try {
      client = clientRepository.findById(clientPatio.getClientDto().getId());
    } catch (Exception e) {
      throw new IllegalArgumentException("El id del cliente no puede ser nulo");
    }
    return client.orElseThrow(
        () -> new EntityNotFoundException("El cliente no existe"));
  }

  @Override
  public void deleteClientPatioRelationship(Long id) {
    ClientPatioRelationship relationship = getRelationshipIfExist(id);
    verifyAssociatedData(relationship);
    clientPatioRepository.deleteById(relationship.getId());
  }

  private void verifyAssociatedData(ClientPatioRelationship relationship) {
    long count = creditSolicitudeRepository.countByClient_IdAndPatio_IdAndStatusRegistered(
        relationship.getClient().getId(), relationship.getPatio().getId());
    if (count > 0) {
      throw new ConflictOperationException(
          "No se puede eliminar la relacion patio-cliente porque tiene solicitudes de credito registradas");
    }
  }

  @Override
  public ClientPatioDto updateClientPatioRelationship(ClientPatioDto relationship) {
    ClientPatioRelationship clientPatioRelationship;
    ClientPatioRelationship databaseRelationship;
    databaseRelationship = getRelationshipIfExist(relationship.getId());
    getClientIfExists(relationship);
    getPatioIfExists(relationship);
    verifyAssociatedData(databaseRelationship);
    clientPatioRelationship = clientPatioMapper.toEntity(relationship);
    clientPatioRelationship = clientPatioRepository.save(clientPatioRelationship);
    return clientPatioMapper.toDto(clientPatioRelationship);
  }
}
