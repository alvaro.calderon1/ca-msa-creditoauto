package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.ClientPatioDto;

public interface ClientPatioService {

  ClientPatioDto getClientsPatioRelationship(Long idRelationship);

  ClientPatioDto assignClientToPatio(ClientPatioDto clientePatio);

  void deleteClientPatioRelationship(Long idRelationship);

  ClientPatioDto updateClientPatioRelationship(ClientPatioDto relationship);
}
