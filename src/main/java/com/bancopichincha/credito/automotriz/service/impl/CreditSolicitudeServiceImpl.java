package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.Client;
import com.bancopichincha.credito.automotriz.domain.ClientPatioRelationship;
import com.bancopichincha.credito.automotriz.domain.CreditSolicitude;
import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus;
import com.bancopichincha.credito.automotriz.domain.emuns.VehicleStatus;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.repository.ClientPatioRepository;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.CreditSolicitudeRepository;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.CreditSolicitudeService;
import com.bancopichincha.credito.automotriz.service.dto.CreditSolicitudeDto;
import com.bancopichincha.credito.automotriz.service.mapper.CreditSolicitudeMapper;
import java.time.LocalDate;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreditSolicitudeServiceImpl implements CreditSolicitudeService {

  private final CreditSolicitudeRepository creditSolicitudeRepository;
  private final ClientPatioRepository clientPatioRepository;
  private final VehicleRepository vehicleRepository;
  private final ExecutiveRepository executiveRepository;
  private final PatioRepository patioRepository;
  private final ClientRepository clientRepository;
  private final CreditSolicitudeMapper mapper;


  @Override
  public CreditSolicitudeDto createCreditSolicitude(CreditSolicitudeDto creditSolicitudeDto) {
    Client client;
    Patio patio;
    Vehicle vehicle;
    Executive executive;
    CreditSolicitude creditSolicitude;
    client = getClientIfExist(creditSolicitudeDto.getClient().getId());
    patio = getPatioIfExist(creditSolicitudeDto.getPatio().getId());
    vehicle = getVehicleIfExist(creditSolicitudeDto.getVehicle().getId());
    executive = getExecutiveIfExist(creditSolicitudeDto.getExecutive().getId());
    verifyTodaysSolicitudes(creditSolicitudeDto);
    verifyCarsSolicitudeHistorial(creditSolicitudeDto);
    verifyClientPatioRelationship(client, patio);
    creditSolicitude = mapper.toEntity(creditSolicitudeDto);
    assignStatus(creditSolicitude, creditSolicitudeDto);
    creditSolicitudeRepository.save(creditSolicitude);
    updateVehicleStatus(vehicle);
    return mapper.toDto(creditSolicitude);
  }

  private void updateVehicleStatus(Vehicle vehicle) {
    vehicle.setStatus(VehicleStatus.IN_CREDIT);
    vehicleRepository.save(vehicle);
  }

  private void assignStatus(CreditSolicitude creditSolicitude,
      CreditSolicitudeDto creditSolicitudeDto) {
    switch (creditSolicitudeDto.getStatus()) {
      case "R":
        creditSolicitude.setStatus(CreditSolicitudeStatus.REGISTERED);
        break;
      case "A":
        creditSolicitude.setStatus(CreditSolicitudeStatus.DISPATCHED);
        break;
      case "D":
        creditSolicitude.setStatus(CreditSolicitudeStatus.REJECTED);
        break;
      default:
        throw new ConflictOperationException("El estado de la solicitud no es valido");
    }
  }

  private Executive getExecutiveIfExist(Long id) {
    Optional<Executive> person;
    try {
      person = executiveRepository.findById(id);
    } catch (Exception e) {
      throw new ConflictOperationException("El id del ejecutivo no puede ser nulo");
    }
    return person.orElseThrow(() -> new ConflictOperationException("El ejecutivo no existe"));
  }

  private Vehicle getVehicleIfExist(Long id) {
    Optional<Vehicle> person;
    try {
      person = vehicleRepository.findById(id);
    } catch (Exception e) {
      throw new ConflictOperationException("El id del vehiculo no puede ser nulo");
    }
    return person.orElseThrow(() -> new ConflictOperationException("El vehiculo no existe"));
  }

  private Patio getPatioIfExist(Long id) {
    Optional<Patio> person;
    try {
      person = patioRepository.findById(id);
    } catch (Exception e) {
      throw new ConflictOperationException("El id del patio no puede ser nulo");
    }
    return person.orElseThrow(() -> new ConflictOperationException("El patio no existe"));
  }

  private Client getClientIfExist(Long id) {
    Optional<Client> person;
    try {
      person = clientRepository.findById(id);
    } catch (Exception e) {
      throw new ConflictOperationException("El id del cliente no puede ser nulo");
    }
    return person.orElseThrow(() -> new ConflictOperationException("El cliente no existe"));
  }

  private void verifyClientPatioRelationship(Client client, Patio patio) {
    ClientPatioRelationship relationship;
    if (!clientPatioRepository.existsByClient_IdAndPatio_Id(
        client.getId(), patio.getId())) {
      relationship = new ClientPatioRelationship(null, LocalDate.now(), patio, client);
      clientPatioRepository.save(relationship);
    }
  }

  private void verifyCarsSolicitudeHistorial(CreditSolicitudeDto creditSolicitudeDto) {
    if (creditSolicitudeRepository.existsByVehicle_IdAndActiveSolicitude(
        creditSolicitudeDto.getVehicle().getId())) {
      throw new ConflictOperationException(
          "El auto que envio para la solicitud de credito ya esta reservado para otro");
    }
  }

  private void verifyTodaysSolicitudes(CreditSolicitudeDto creditSolicitudeDto) {
    if (creditSolicitudeRepository.countTodayActiveSolicitudes(
        creditSolicitudeDto.getClient().getId(), LocalDate.parse(creditSolicitudeDto.getDate()))
        > 0) {
      throw new ConflictOperationException(
          "Ya existe una solicitud de crédito activa para el cliente el dia de hoy");
    }
  }
}
