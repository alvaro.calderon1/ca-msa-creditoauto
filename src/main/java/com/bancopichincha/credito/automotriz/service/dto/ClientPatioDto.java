package com.bancopichincha.credito.automotriz.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientPatioDto {

  private final Long id;
  @JsonProperty("clienteDto")
  @NotNull(message = "El cliente no puede ser nulo")
  private final ClientDto clientDto;
  @JsonProperty("patioDto")
  @NotNull(message = "El patio no puede ser nulo")
  private final PatioDto patioDto;
  @JsonProperty("fechaAsignacion")
  @NotNull(message = "La fecha de asignación no puede ser nula")
  private final String assignedAt;

}
