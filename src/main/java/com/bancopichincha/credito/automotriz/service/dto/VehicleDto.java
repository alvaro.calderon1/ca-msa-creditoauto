package com.bancopichincha.credito.automotriz.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.Vehicle} entity
 */
@Data
@Builder
public class VehicleDto implements Serializable {

  private final Long id;
  @JsonProperty("placa")
  @NotBlank(message = "La placa no puede ser nula o vacía")
  private final String plate;
  @JsonProperty("modelo")
  @NotEmpty(message = "El modelo no puede ser nulo o vacío")
  private final String model;
  @JsonProperty("numeroChasis")
  @NotBlank(message = "El número de chasis no puede ser nulo o vacío")
  private final String chassisNumber;
  @JsonProperty("marcaDto")
  @NotNull(message = "La marca no puede ser nula")
  private final BrandDto brand;
  @JsonProperty("tipo")
  private final String type;
  @JsonProperty("cilindraje")
  @NotBlank(message = "El cilindraje no puede ser nulo o vacío")
  private final String displacement;
  @JsonProperty("avaluo")
  @NotBlank(message = "El avalúo no puede ser nulo o vacío")
  private final String appraisal;
  @JsonProperty("estado")
  private final String status;
  @JsonProperty("fecha")
  private final String fabricationDate;
}