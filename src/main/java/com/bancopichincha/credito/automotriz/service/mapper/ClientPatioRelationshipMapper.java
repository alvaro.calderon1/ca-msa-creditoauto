package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.ClientPatioRelationship;
import com.bancopichincha.credito.automotriz.service.dto.ClientPatioDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = ComponentModel.SPRING)
public interface ClientPatioRelationshipMapper {

  @Mapping(source = "patioDto", target = "patio")
  @Mapping(source = "clientDto", target = "client")
  @Mapping(source = "assignedAt", target = "assignedAt", dateFormat = "yyyy-MM-dd")
  ClientPatioRelationship toEntity(ClientPatioDto clientPatioDto);

  @Mapping(target = "patioDto", source = "patio")
  @Mapping(target = "clientDto", source = "client")
  @Mapping(target = "assignedAt", source = "assignedAt", dateFormat = "yyyy-MM-dd")
  ClientPatioDto toDto(ClientPatioRelationship clientPatioRelationship);

}