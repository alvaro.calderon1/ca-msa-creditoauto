package com.bancopichincha.credito.automotriz.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;


@Getter
@Setter
@AllArgsConstructor
public class ApiErrorDto {

  private HttpStatus status;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
  private LocalDateTime timestamp;
  private List<String> errors;

  private ApiErrorDto() {
    timestamp = LocalDateTime.now();
  }

  public ApiErrorDto(HttpStatus status, List<String> error) {
    this();
    this.status = status;
    this.errors = error;
  }
}
