package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CreditSolicitude;
import com.bancopichincha.credito.automotriz.service.dto.CreditSolicitudeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = ComponentModel.SPRING)
public interface CreditSolicitudeMapper {

  @Mapping(target = "installments", source = "quotas")
  @Mapping(target = "deadlineMonths", source = "months")
  @Mapping(target = "createdAt", source = "date")
  @Mapping(target = "amount", source = "entry")
  @Mapping(target = "status", source = "status", ignore = true)
  CreditSolicitude toEntity(CreditSolicitudeDto patioDto);

  @Mapping(source = "installments", target = "quotas")
  @Mapping(source = "deadlineMonths", target = "months")
  @Mapping(source = "createdAt", target = "date")
  @Mapping(source = "amount", target = "entry")
  CreditSolicitudeDto toDto(CreditSolicitude patio);

}
