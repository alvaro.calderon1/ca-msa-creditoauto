package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.Brand} entity
 */
@Data
public class BrandDto implements Serializable {

  @NotNull(message = "El id no puede ser nulo")
  @Min(value = 1, message = "El id no puede ser menor a 1")
  private final Long id;
  private final String name;
}