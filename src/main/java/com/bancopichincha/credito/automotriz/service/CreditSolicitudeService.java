package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CreditSolicitudeDto;

public interface CreditSolicitudeService {

  CreditSolicitudeDto createCreditSolicitude(CreditSolicitudeDto creditSolicitudeDto);

}
