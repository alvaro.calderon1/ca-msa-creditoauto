package com.bancopichincha.credito.automotriz.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class CreditSolicitudeDto {

  @JsonProperty("id")
  private Long id;
  @JsonProperty("clientDto")
  @NotNull(message = "El cliente no puede ser nulo")
  private ClientDto client;
  @JsonProperty("patioDto")
  @NotNull(message = "El patio no puede ser nulo")
  private PatioDto patio;
  @JsonProperty("ejecutivoDto")
  @NotNull(message = "El ejecutivo no puede ser nulo")
  private ExecutiveDto executive;
  @JsonProperty("vehiculoDto")
  @NotNull(message = "El vehiculo no puede ser nulo")
  private VehicleDto vehicle;
  @JsonProperty("fechaElaboracion")
  @NotNull(message = "La fecha de elaboracion no puede ser nula")
  private String date;
  @JsonProperty("mesesPlazo")
  @NotNull(message = "El plazo no puede ser nulo")
  @Min(value = 1, message = "El plazo no puede ser menor a 1")
  private Integer months;
  @JsonProperty("cuotas")
  @NotNull(message = "Las cuotas no pueden ser nulas")
  @Min(value = 1, message = "Las cuotas no pueden ser menores a 1")
  private Integer quotas;
  @JsonProperty("entrada")
  @NotNull(message = "La entrada no puede ser nula")
  @Min(value = 1, message = "La entrada no puede ser menor a 1")
  private Long entry;
  @JsonProperty("observacion")
  private String observations;
  @JsonProperty("estadoSolicitudCredito")
  @NotNull(message = "El estado no puede ser nulo")
  private String status;
}
