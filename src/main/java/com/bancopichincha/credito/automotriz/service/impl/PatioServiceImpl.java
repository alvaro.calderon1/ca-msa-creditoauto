package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.exception.ConflictOperationException;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioMapper;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class PatioServiceImpl implements PatioService {

  private final PatioRepository patioRepository;
  private final PatioMapper patioMapper;

  @Override
  public PatioDto createPatio(PatioDto patioDto) {
    Patio patio, patioCreated;
    patio = patioMapper.toEntity(patioDto);
    patioCreated = patioRepository.save(patio);
    return patioMapper.toDto(patioCreated);
  }

  @Override
  public PatioDto updatePatio(PatioDto patioDto) {
    Patio patio, patioUpdated;
    getPatioIfExists(patioDto.getId());
    patio = patioMapper.toEntity(patioDto);
    patioUpdated = patioRepository.save(patio);
    return patioMapper.toDto(patioUpdated);
  }

  public Patio getPatioIfExists(Long id) {
    Optional<Patio> patio;
    try {
      patio = patioRepository.findById(id);
    } catch (Exception e) {
      throw new IllegalArgumentException("El id del patio no puede ser nulo");
    }
    return patio.orElseThrow(() -> new EntityNotFoundException("El patio no existe"));
  }

  @Override
  public void deletePatio(Long id) {
    Patio patio = getPatioIfExists(id);
    if (!patio.getClients().isEmpty()) {
      throw new ConflictOperationException(
          "No se puede eliminar el patio con id: " + id + " tiene referencia con cliente");
    }
    if (!patio.getExecutives().isEmpty()) {
      throw new ConflictOperationException(
          "No se puede eliminar el patio con id: " + id + " tiene referencia con ejecutivo");
    }
    patioRepository.deleteById(id);
  }

  @Override
  public PatioDto getPatio(Long id) {
    Patio patio = getPatioIfExists(id);
    return patioMapper.toDto(patio);
  }
}
