package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.Patio;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


/**
 * A DTO for the {@link Patio} entity
 */
@Data
@Builder
@AllArgsConstructor
public class PatioDto implements Serializable {

  private final Long id;
  @NotBlank(message = "El nombre no puede ser nulo o vacío")
  @JsonProperty("nombre")
  private final String name;
  @NotBlank(message = "La dirección no puede ser nula o vacía")
  @JsonProperty("direccion")
  private final String address;
  @NotBlank(message = "El teléfono no puede ser nulo o vacío")
  @JsonProperty("telefono")
  private final String phone;
  @Min(value = 1, message = "El número de POS debe ser mayor a 0")
  @NotNull(message = "El número de POS no puede ser nulo")
  @JsonProperty("numeroPuntoVenta")
  private final Integer numberPos;
}