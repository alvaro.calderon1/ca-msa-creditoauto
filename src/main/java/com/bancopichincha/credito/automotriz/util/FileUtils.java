package com.bancopichincha.credito.automotriz.util;

import java.io.IOException;
import java.util.List;
import org.aspectj.util.FileUtil;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


public class FileUtils {

  public static List<String> getResourceFileLines(String path) throws IOException {
    List<String> lines;
    Resource resource;
    resource = new ClassPathResource(path);
    return FileUtil.readAsLines(resource.getFile());
  }
}
