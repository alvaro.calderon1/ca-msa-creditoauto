package com.bancopichincha.credito.automotriz.domain;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
public class Executive extends Person {

  @ManyToOne
  @JoinColumn(name = "patio_id")
  private Patio patio;

  public Executive(Long id, String identification, String name, String lastName, String address,
      String phone, String cellphone, Integer age, Patio patio) {
    super(id, identification, name, lastName, address, phone, cellphone, age);
    this.patio = patio;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    Executive executive = (Executive) o;
    return getId() != null && Objects.equals(getId(), executive.getId());
  }

}
