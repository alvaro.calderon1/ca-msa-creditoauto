package com.bancopichincha.credito.automotriz.domain;


import com.bancopichincha.credito.automotriz.domain.emuns.MaritalStatus;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class Client extends Person {

  @Enumerated(EnumType.STRING)
  private MaritalStatus maritalStatus;

  private String spouseIdentification;

  private String spouseName;

  private Boolean isSubjectToCredit;

  private LocalDate birthDate;

  @OneToMany
  private Set<ClientPatioRelationship> patios;

  public Client(Long id, String identification, String name, String lastName, String address,
      String phone, String cellphone, Integer age, MaritalStatus maritalStatus,
      String spouseIdentification, String spouseName, Boolean isSubjectToCredit,
      LocalDate birthDate) {
    super(id, identification, name, lastName, address, phone, cellphone, age);
    this.maritalStatus = maritalStatus;
    this.spouseIdentification = spouseIdentification;
    this.spouseName = spouseName;
    this.isSubjectToCredit = isSubjectToCredit;
    this.birthDate = birthDate;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    Client client = (Client) o;
    return getId() != null && Objects.equals(getId(), client.getId());
  }

}
