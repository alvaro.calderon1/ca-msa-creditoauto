package com.bancopichincha.credito.automotriz.domain.emuns;

public enum CreditSolicitudeStatus {
  REGISTERED,
  DISPATCHED,
  REJECTED
}
