package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.emuns.CreditSolicitudeStatus;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditSolicitude {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(nullable = false)
  private LocalDate createdAt;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Client client;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Vehicle vehicle;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Patio patio;

  @Column(nullable = false)
  private Integer deadlineMonths;

  @Column(nullable = false)
  private Integer installments;

  @Column(nullable = false)
  private BigDecimal amount;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Executive executive;

  private String observations;

  @Enumerated(EnumType.STRING)
  private CreditSolicitudeStatus status;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    CreditSolicitude that = (CreditSolicitude) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
