package com.bancopichincha.credito.automotriz.domain.emuns;

public enum VehicleStatus {
  REGISTERED("R"), SOLD("V"), IN_PARKING("P"), IN_CREDIT("C");
  private final String abbreviation;

  VehicleStatus(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return abbreviation;
  }
}
