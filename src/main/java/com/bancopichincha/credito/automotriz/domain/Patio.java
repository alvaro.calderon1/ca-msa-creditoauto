package com.bancopichincha.credito.automotriz.domain;

import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;


@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class Patio {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;
  @Column(nullable = false)
  private String name;
  @Column(nullable = false)
  private String address;
  @Column(nullable = false)
  private String phone;
  @Column(nullable = false)
  private Integer numberPos;
  @OneToMany(mappedBy = "patio", targetEntity = ClientPatioRelationship.class)
  private Set<ClientPatioRelationship> clients;
  @OneToMany(mappedBy = "patio", targetEntity = Executive.class)
  private Set<Executive> executives;

  public Patio(Long id, String name, String address, String phone, Integer numberPos) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.phone = phone;
    this.numberPos = numberPos;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    Patio patio = (Patio) o;
    return getId() != null && Objects.equals(getId(), patio.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
