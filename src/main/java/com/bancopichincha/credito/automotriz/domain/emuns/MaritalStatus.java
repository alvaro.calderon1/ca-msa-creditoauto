package com.bancopichincha.credito.automotriz.domain.emuns;

public enum MaritalStatus {
  SINGLE,
  MARRIED,
  DIVORCED,
  WIDOWED
}
