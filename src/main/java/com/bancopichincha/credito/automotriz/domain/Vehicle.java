package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.emuns.VehicleStatus;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
public class Vehicle {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;
  @Column(nullable = false, unique = true)
  private String plate;
  @Column(nullable = false)
  private String model;
  @Column(nullable = false)
  private String chassisNumber;
  @ManyToOne
  private Brand brand;
  private String type;
  @Column(nullable = false)
  private String displacement;
  @Column(nullable = false)
  private String appraisal;
  @Enumerated(EnumType.STRING)
  private VehicleStatus status;
  private String fabricationDate;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    Vehicle vehicle = (Vehicle) o;
    return getId() != null && Objects.equals(getId(), vehicle.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  public boolean isStatus(VehicleStatus status) {
    return this.status.equals(status);
  }
}
